import { useLayoutEffect, useRef } from 'react';

const useDidMountLayoutEffect = (func, deps) => {
    const didMount = useRef(false);

    useLayoutEffect(() => {
        if (didMount.current) func();
        else didMount.current = true;
    }, deps);
}

export default useDidMountLayoutEffect;