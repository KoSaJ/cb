import React from 'react';
import Home from '../Pages/Home';
import Search from '../Pages/Search';
import Wishlist from '../Pages/Wishlist';
import ErrorPage from '../Pages/ErrorPage';
import {Switch, Route} from 'react-router-dom';

const Page = () => {
    return ( 
    <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/search" component={Search} />
        <Route path="/wishlist" component={Wishlist} />
        <Route component={ErrorPage} />
    </Switch> );
}
 
export default Page;