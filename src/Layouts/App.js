import React from 'react';
import {BrowserRouter as Router} from 'react-router-dom';
import Nav from './Nav'
import Page from './Page'
import Footer from './Footer'
import '../styles/App.css';

function App () {
  return (
    <Router>
      <div className="App">
      <Nav />
      <Page />
      <Footer />
      </div>
    </Router>
  );
}

export default App;
