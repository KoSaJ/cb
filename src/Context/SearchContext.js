import React, { useState, useContext } from "react";
import useDidMountEffect from '../Hooks/useDidMountEffect';

export const SearchContext = React.createContext();

const SearchProvider = ({children}) => {
    const [page, setPage] = useState(1);
    const [title, setTitle] = useState('');
    const [results, setResults] = useState();
    

    async function fetchTitles() {
        const response = await fetch(`http://www.omdbapi.com/?apikey=d0f08059&s=${title}&page=${page}`);
        const json = await response.json();
        
        if(json !== 'undefined')
            setResults(json);
    }

    useDidMountEffect(() =>
    {
        if(title !== '' && title !== 'undefined') 
            fetchTitles();
    }, [title, page])

    const value = {
        inputProps: {
            page: {
                value: page,
            onChange: (e) => setPage(e.target.value),
            },
            title: {
                value: title,
            onChange: (e) => setTitle(e.target.value),
          }
        },
        results,
        resetSearch: (event) => {
            event.preventDefault();

            setPage(1);
            setTitle('');
            setResults('undefined');
        }
      };

    return (
        <SearchContext.Provider value={value}>
            {children}
        </SearchContext.Provider>
        );
}

export const useSearch = () => {
    const context = useContext(SearchContext);
  
    if (context === undefined) {
      throw new Error("useSearch should be nested in SearchProvider!");
    }
  
    return context;
  };
 
export default SearchProvider;