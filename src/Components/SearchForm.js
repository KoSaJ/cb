import React from 'react';
import { useSearch } from '../Context/SearchContext';


const SearchForm = () => {
    const { inputProps, resetSearch } = useSearch();

    return ( 
        <form onSubmit={resetSearch}>
            <input {...inputProps.title} type="text" name="title" placeholder="Title..."/>
            <button type="submit" value="Reset">Clear Search</button>
        </form> 
    );
}
 
export default SearchForm;