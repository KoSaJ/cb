import React from 'react';
import { useCookies } from 'react-cookie';
import { useSearch } from '../Context/SearchContext';

const SearchDisplay = () => {
    const { inputProps, results } = useSearch();
    const [cookies, setCookie, removeCookie] = useCookies(['wishlist']);

    const handleWishlistAdd = (e) => {
        const imdbid = e.target.getAttribute('imdbid');

        if(typeof cookies.wishlist !== "undefined" || cookies.wishlist === ""){
            if(cookies.wishlist.some(item => item.imdbID === imdbid))
            {
                alert(`Item with id=${imdbid} was already stored.`);
                return;
            }

            const result = results.Search.find(item => item.imdbID === imdbid)
            setCookie('wishlist', [...cookies.wishlist, result], { path: '/' });
        }
        else {
            const result = results.Search.find(item => item.imdbID === imdbid)
            setCookie('wishlist', [result], { path: '/' });
        }
    }

    const RenderTable = () => {

        if(typeof results == "undefined" || typeof results.Search == "undefined"){
            return;
        }

        return (
            <table>
                <tbody>
                    <tr>
                        <th>Title</th>
                        <th>Year</th>
                        <th>Type</th>
                        <th>Wishlist</th>
                    </tr>
                    {results.Search.map(item => 
                    <tr key={item.imdbID}>
                        <td>{item.Title}</td>
                        <td>{item.Year}</td>
                        <td>{item.Type}</td>
                        <td>
                            <button imdbid={item.imdbID} onClick={handleWishlistAdd}>Add</button>
                        </td>
                    </tr>)}
                </tbody>  
            </table>
        );
    }

    return (
        <div>
            {RenderTable()}
            <button onClick={() => removeCookie('wishlist')}>Clear cookies</button>
        </div>
    );
}
 
export default SearchDisplay;