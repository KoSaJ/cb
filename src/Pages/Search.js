import React from 'react';
import SearchForm from '../Components/SearchForm';
import SearchDisplay from '../Components/SearchDisplay';
import SearchProvider from '../Context/SearchContext';

const Search = () => {
    return (
        <SearchProvider>
            <SearchForm />
            <SearchDisplay />
        </SearchProvider>
    );
}
 
export default Search;