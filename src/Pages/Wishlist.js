import React from 'react';
import { useCookies } from 'react-cookie';

const Wishlist = () => {
    const [cookies, setCookie] = useCookies(['wishlist']);

const handleWishlistRemove = (e) => {
    const imdbid = e.target.getAttribute('imdbid');

    setCookie('wishlist', cookies.wishlist.filter(item => item.imdbID !== imdbid));
}

    const RenderTable = () => {

        if(typeof cookies.wishlist != "undefined" && cookies.wishlist != null && cookies.wishlist.length != null && cookies.wishlist.length > 0){
        

        return (
            <table>
                <tbody>
                    <tr>
                        <th>Title</th>
                        <th>Year</th>
                        <th>Type</th>
                        <th>Wishlist</th>
                    </tr>
                    {cookies.wishlist.map(item => 
                    <tr key={item.imdbID}>
                        <td>{item.Title}</td>
                        <td>{item.Year}</td>
                        <td>{item.Type}</td>
                        <td>
                            <button imdbid={item.imdbID} onClick={handleWishlistRemove}>Remove</button>
                        </td>
                    </tr>)}
                </tbody>  
            </table>
        );
                    }
                    return;
    }

return ( <div>{RenderTable()}</div> );
}
 
export default Wishlist;