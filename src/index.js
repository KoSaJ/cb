import React from 'react';
import ReactDOM from 'react-dom';
import App from './Layouts/App';
import './styles/index.css';

ReactDOM.render(<App />, document.getElementById('root'));
